# Descripción del repositorio

En este repositorio se llevaron a cabo varios procedimientos a través de la linea de comandos.

Primero se cambio el nombre de los archivos de la carpeta "fake" reemplazando "mid" por "img", luego se creo un archivo que especifica el cambio de nombre de cada uno de los archivos. Después se realizó el mismo proceso con os archivos de la carpeta "real".

El siguiente paso fue crear una carpeta llamada "dataSet" a la que se movieron todos los archivos de las carpetas "real" y "fake".

Por ultimo, se investigó el comando tar y se lo utilizó para crear un archivo backup comprimido de la carpeta “dataSet”.