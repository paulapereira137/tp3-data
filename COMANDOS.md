# Comandos

En este archivo se encuentran detallados los comandos utilizados en el proceso descripto en README.md 

Estando en  tp3-data/fake se utilizaron los siguientes comandos:

> $ for nombre in *.jpg

>  do

>  echo mv $nombre img_$nombre >> script.sh

>  done

Con esto se creo un archivo que conten�a un scrpit, en �l se leia 'mv mid_numeroimagen.jpg img_mid_numeroimagen.jpg' para cada una de las imagenes

Luego se utiliz�:

> $ cut -d "_" -f 1,2,3,5,6 script.sh > script_1.sh

Esto permiti� crear un nuevo archivo script en el que se le�a 'mv mid_numeroimagen.jpg img_numeroimagen.jpg' en cada linea, es decir, se consigu�o eliminar el mid.

Luego se ejecut�:

> $ bash script_1.sh

Esta linea ejecut� el script, por lo que finalmente se reenombraron los archivos.

A continuaci�n se llev� a cabo el proceso de generar un archivo que detallara el cambio de nombre de todos los archivos. Para esto solo fue necesaria la siguiente linea:

> $cut -d " " -f 2,3 --output-delimiter=$' > ' script_1.sh > renameFake.dat

Lo que caus� esto fue que se creara un archivo renameFake.dat a apartir del archivo script_1.sh que se hab�a ejecutado anteriormente, pero sin el mv del principio de cada linea y reemplazando el espacio intermedio por ' /> '.

Luego, con los siguientes comandos me traslad� a la carpeta real:

> $ cd ..

> $ cd real

Una vez all�, se repiti� el mismo prosedimiento descripto anteriormente, esta vez con los archivos de la carpeta real.

Se utiliz� la siguiente linea de comandos para volver a la carpeta tp3-data:

> $ cd ..

Y despu�s la siguiente para crear la carpeta dataSet:

> $ mkdir dataSet

Con las siguientes lineas se movieron los archivos de las carpetas fake y real a la carpeta dataSet:

> $ mv fake/*.jpg dataSet

> $ mv fake/*.dat dataSet

> $ mv real/*.jpg dataSet

> $ mv real/*.dat dataSet

A continuaci�n se utilizaron los siguientes comandos:

> $ tar --help

Esto imprim�� en la pantalla informaci�n sobre el comando tar, no se utiliz� el comando man ya que no funcionaba.
A partir de la informaci�n obtenida se tipeo la siguiente linea:

>  $ tar -cvf 15_07_20dataSet.tar dataSet

Esto creo un archivo comprimido .tar con el nombre 15_07_20dataSet a partir de la carpeta dataSet. La bandera -cvf se agreg� para que se cree el archivo empaquetado, se impriman en pantalla las operaciones que se estaban realizando y para que esp�es se pueda trabajar con el archivo.

